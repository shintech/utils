const { createLogger, format, transports } = require('winston')
const { label, splat, combine, colorize, printf, timestamp } = format
const path = require('path')

const PKG_NAME = process.env['npm_package_name']
const PKG_VERSION = process.env['npm_package_version']
const NODE_ENV = process.env['NODE_ENV']

const myFormat = printf(({ splat, level, label, message, timestamp }) => {
  return `${level}: ${label} - ${timestamp} -> ${message}`
})

module.exports = function () {
  let logger = createLogger({
    level: 'info',

    format: combine(
      label({ label: `${PKG_NAME} v:${PKG_VERSION}` }),
      timestamp(),
      splat(),
      myFormat
    )
  })

  logger.add(new transports.File({ filename: path.join('log', `error.log`), level: 'error' }))
  logger.add(new transports.File({ filename: path.join('log', `combined.log`), level: 'info' }))

  if (NODE_ENV === 'development' || NODE_ENV === 'staging') {
    logger.add(
      new transports.Console({
        format: combine(
          colorize(),
          splat(),
          myFormat
        )
      }))
  }

  return logger
}
