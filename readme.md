# shintech-development/seed

## Table of Contents
1. [ Synopsis ](#synopsis)
2. [ Installation/Configuration ](#install) <br />
	a. [.env ](#env) <br />
	b. [Development ](#development) <br />
	c. [Production ](#production)
	c. [git hooks ](#git-hooks)
3. [ API ](#usage) <br />
    a. [/api/home](#API-home) <br />
    b. [/api/page](#API-page) <br />
    c. [/api/posts](#API-posts) <br />
    d. [/api/posts/:id](#API-single) <br />

<a name="synopsis"></a>
### Synopsis

Next.JS + Express.JS
  
### Installation

    ./install.sh

<a name="install"></a>
### Installation/Configuration
<a name="env"></a>
#### Copy the config for the environment from config/env/[environment].env and edit as necessary.

    PORT=8000
    NODE_ENV=development
    BASE_URL=https://domain
    EMAIL=example@example.org
    POSTGRES_URI=postgresURI
    JWT_SECRET=secret
    HOST=localhost
    DOMAIN=domain

<a name="development"></a>
#### Development

    npm run dev

    # or

    yarn dev

<a name="production"></a>
#### Production
    docker-compose build && docker-compose up -d
    
<a name="git-hooks"></a>
#### git hooks
    ln -s /path/to/repo/config/hooks/hook /path/to/repo/.git/hooks/

<a name="usage"></a>
### Usage
<a name="API"></a>
#### API Endpoints
<a name='API-home'></a>
##### /api/home
<a name='API-page'></a>
##### /api/page
<a name='API-posts'></a>
##### /api/posts
<a name='API-single'></a>
##### /api/posts/:id
