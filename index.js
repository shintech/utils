const Logger = require('./logger')

const repeat = n => f => x => {
  if (n > 0) { return repeat(n - 1)(f)(f(x)) } else { return x }
}

const times = n => f =>
  repeat(n)(i => (f(i), i + 1))(0) /* eslint-disable-line */

module.exports = {
  times,
  Logger
}
